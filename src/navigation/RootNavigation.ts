import * as React from 'react';

import { StackActions } from '@react-navigation/native';

export const isAppMountedRef = React.createRef<boolean>();
export const navigationRef = React.createRef<any>();

export function navigate(name: string, params?: any) {
  safetyNavigationCall(() => {
    navigationRef.current?.navigate(name, params);
  });
}

export function push(...args: any) {
  safetyNavigationCall(() => {
    navigationRef.current?.dispatch(StackActions.push(...args));
  });
}

export function pop(...args: any) {
  safetyNavigationCall(() => {
    navigationRef.current?.dispatch(StackActions.pop(...args));
  });
}

export function dispatch(action: any) {
  safetyNavigationCall(() => {
    navigationRef.current?.dispatch(action);
  });
}

function safetyNavigationCall(successCallback: any) {
  if (canUseNavigation()) {
    successCallback();
  } else {
    // You can decide what to do if the app hasn't mounted
    // You can ignore this, or add these actions to a queue you can call later
    setTimeout(() => { safetyNavigationCall(successCallback); }, 100);
  }
}

function canUseNavigation() {
  return !!(isAppMountedRef.current && navigationRef.current);
}
