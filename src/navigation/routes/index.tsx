import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import { DRIVERS_INDEX, DRIVER_SHOW } from '../constants';
import DriversPagesIndex from 'src/app/modules/drivers/ui/screens/list';
import DriversPagesShow from 'src/app/modules/drivers/ui/screens/show';
import { Driver } from 'src/app/modules/drivers/domain/interfaces/Driver';

const Stack = createStackNavigator();

const StackNavigator = () => {
  return (
    <Stack.Navigator initialRouteName={DRIVERS_INDEX}>
      <Stack.Screen options={{ headerShown: false }} name={DRIVERS_INDEX} component={DriversPagesIndex} />
      <Stack.Screen 
        name={DRIVER_SHOW}
        component={DriversPagesShow}
        options={({ route }) => ({ 
          title: (route.params?.driver as Driver)?.givenName,
          headerBackTitle: 'Back'
         })}
        // options={{ title: 'Driver detail' }} 
      />
    </Stack.Navigator>
  );
};
export default StackNavigator;
