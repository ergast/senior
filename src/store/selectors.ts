import { RootState } from "src/store/rootReducer";
import { SubCategory } from "src/modules/categories/domain/interfaces/Category";
import { subCategoriesEntitiesSelector } from "src/modules/categories/store/selectors";

export const selectByIds = (state: RootState, ids: [string]): [SubCategory] =>
  ids.map((id: string) => {
    return subCategoriesEntitiesSelector(state, id)
  });
