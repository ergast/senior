import { combineReducers } from '@reduxjs/toolkit'
import driversReducer from '../app/modules/drivers/store';
import racesReducer from 'src/app/modules/races/store';

const rootReducer = combineReducers({
  drivers: driversReducer,
  races: racesReducer,
});

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
