import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit'
import rootReducer, { RootState } from './rootReducer'

const store = configureStore({
  reducer: rootReducer
})

export type AppDispatch = typeof store.dispatch;
export type AppState = typeof store.getState;
export type AppThunk = ThunkAction<void, RootState, null, Action>;
export default store;