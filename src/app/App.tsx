import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import FlashMessage from 'react-native-flash-message';
import { isAppMountedRef, navigationRef } from 'src/navigation/RootNavigation';
import store from 'src/store/store';
import StackNavigator from 'src/navigation/routes';

const App = () => {

  useEffect(() => {
    (isAppMountedRef as any).current = true;
    return () => {
      (isAppMountedRef as any).current = false;
    }
  }, []);

  return (
    <Provider store={store}>
      <NavigationContainer ref={navigationRef}>
        <StackNavigator />
        <FlashMessage position="top" />
      </NavigationContainer>
    </Provider>
  );
};

export default App;