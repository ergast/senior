import { combineReducers } from '@reduxjs/toolkit'
import indexReducer from './index/reducer';

const racesReducer = combineReducers({
  index: indexReducer
});

export type RacesState = ReturnType<typeof racesReducer>

export default racesReducer;
