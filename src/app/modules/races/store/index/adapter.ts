import { createEntityAdapter } from "@reduxjs/toolkit";
import { Race } from "../../domain/interfaces/Race";

export const racesAdapter = createEntityAdapter<Race>({
  selectId: race => `${race.date}_${race.raceName}`,
});