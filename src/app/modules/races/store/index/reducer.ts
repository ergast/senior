import { createSlice } from "@reduxjs/toolkit";
import { racesAdapter } from "./adapter";
import { loadRaces, refreshRaces, loadNextPage } from "./actions";
import { APIResponseMeta } from "src/app/modules/api/interfaces/APIResponseMeta";
import { Race } from "../../domain/interfaces/Race";

// Todo move to List + pagination slice factory
export const RacesEntitiesSlice = createSlice({
  name: 'Races',
  initialState: racesAdapter.getInitialState({
    isLoading: false,
    isRefreshing: false,
    meta: {} as APIResponseMeta
  }),
  reducers: {},
  extraReducers: builder => {
    
    // Load
    builder.addCase(loadRaces.fulfilled, (state, action) => {
      racesAdapter.addMany(state, action.payload.data);
      state.meta = action.payload.meta;
      state.isLoading = false;
    });
    builder.addCase(loadRaces.pending, (state, action) => {
      state.isLoading = true;
    });
    builder.addCase(loadRaces.rejected, (state, action) => {
      state.isLoading = false;
    });

    // Refresh
    builder.addCase(refreshRaces.fulfilled, (state, action) => {
      racesAdapter.addMany(state, action.payload.data);
      state.meta = action.payload.meta;
      state.isRefreshing = false;
    });
    builder.addCase(refreshRaces.pending, (state, action) => {
      state.isRefreshing = true;
    });
    builder.addCase(refreshRaces.rejected, (state, action) => {
      state.isRefreshing = false;
    });

    // Load more
    builder.addCase(loadNextPage.fulfilled, (state, action) => {
      if (action.payload != null) {
        racesAdapter.addMany(state, (action.payload.data as Race[]));
        state.meta = action.payload.meta;
        console.log('action.payload.meta', action.payload.meta);
      }
      state.isLoading = false;
    });
    builder.addCase(loadNextPage.pending, (state, action) => {
      state.isLoading = true;
    });
    builder.addCase(loadNextPage.rejected, (state, action) => {
      state.isLoading = false;
    });
  }
});

const RacesEntitiesReducer = RacesEntitiesSlice.reducer;
export default RacesEntitiesReducer;
