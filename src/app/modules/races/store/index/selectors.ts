import { createSelector } from "@reduxjs/toolkit";

import { RootState } from "src/store/rootReducer";
import { racesStateSelector } from "../selectors";
import { racesAdapter } from "./adapter";

export const racesIndexStateSelector = createSelector(
  racesStateSelector,
  (state) => state.index
)

export const isLoadingSelector = createSelector(
  racesIndexStateSelector,
  (state) => state.isLoading
)

export const isRefreshingSelector = createSelector(
  racesIndexStateSelector,
  (state) => state.isRefreshing
)

export const metaSelector = createSelector(
  racesIndexStateSelector,
  (state) => state.meta
)

export const racesTotalSelector = createSelector(
  metaSelector,
  (state) => (state.total != null) ? Number(state.total) : state.total
)

export const racesSelector = racesAdapter.getSelectors<RootState>(racesIndexStateSelector).selectAll;
export const selectRaceById = racesAdapter.getSelectors<RootState>(racesIndexStateSelector).selectById;
