import { createAsyncThunk } from "@reduxjs/toolkit";
import { Race } from "../../domain/interfaces/Race";
import raceRepository from "../../domain/api/RaceRepository";
import { BaseResponse } from "src/app/modules/api/interfaces/BaseResponse";
import { metaSelector, isRefreshingSelector } from "./selectors";
import { APIResponseMeta } from "src/app/modules/api/interfaces/APIResponseMeta";
import { RootState } from "src/store/rootReducer";
import { canLoadMore } from "src/app/modules/api/helpers";

export const loadRaces = createAsyncThunk<BaseResponse<Race[]>, { offset?: number, limit?: number }>(
  'races/index/load',
  async (params = { offset: 0, limit: 20 }, { dispatch }) => {
    return await raceRepository.load(params);
  }
);

export const loadNextPage = createAsyncThunk<BaseResponse<Race[]|null>, { offset?: number, limit?: number }>(
  'races/index/load_next_page',
  async (params, { dispatch, getState, rejectWithValue }) => {
    const state = getState() as RootState;
    const isRefreshing = isRefreshingSelector(state);
    if (!isRefreshing) {
      const meta = metaSelector(state) as APIResponseMeta;
      const canLoad = canLoadMore(meta);
      if (canLoad) {
        const loadParams = { limit: meta.limit, offset: Number(meta.offset) + Number(meta.limit) };
        return await raceRepository.load(loadParams);
      }
    }
    return rejectWithValue(null);
  }
);

export const refreshRaces = createAsyncThunk<BaseResponse<Race[]>>(
  'races/index/refresh',
  async (params, { dispatch }) => {
    return await raceRepository.load(params);
  }
);