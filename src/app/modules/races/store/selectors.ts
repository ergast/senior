import { RootState } from "src/store/rootReducer";

export const racesStateSelector = (state: RootState) => state.races;