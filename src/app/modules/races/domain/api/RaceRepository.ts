import { BaseRepository } from "src/app/modules/api/BaseRepository";
import { AxiosResponse } from "axios";
import { APIResponse } from "src/app/modules/api/interfaces/APIResponse";
import httpResource from "src/app/modules/api/httpResource";
import { RacesResponse } from "../interfaces/RacesResponse";
import { Race } from "../interfaces/Race";
import { BaseResponse } from "src/app/modules/api/interfaces/BaseResponse";

class RaceRepository extends BaseRepository<RacesResponse, Race> {

  public loadDriverRaces(driverId: string | number, params?: any):Promise<BaseResponse<Race[]>> {
    return this.resource.get(`drivers/${driverId}/${this.relativePath}`, { params })
      .then((response: AxiosResponse<APIResponse<RacesResponse>>) => {
        const result = this.extractListFromResponse(response);
        return this.processResponse<Race[]>(response, result);
      })
  }

  protected extractListFromResponse(response: AxiosResponse<APIResponse<RacesResponse>>): Race[] {
    return response?.data?.MRData?.RaceTable?.Races;
  }
}

const driverRepository = new RaceRepository(httpResource, 'races');
export default driverRepository;