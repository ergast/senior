import { APIResponseMeta } from "src/app/modules/api/interfaces/APIResponseMeta";
import { Race } from "./Race";

export interface RacesResponse extends APIResponseMeta {
  RaceTable: {
    season: string
    Races: Race[]
  }
}