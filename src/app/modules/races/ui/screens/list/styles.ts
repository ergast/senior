import styled from 'styled-components';

export const Title = styled.Text`
  font-size: 18px;
  font-weight: bold;
  text-align: center;
  color: #1E2133;
`;

export const Subtitle = styled.Text`
  font-size: 14px;
  margin-bottom: 14px;
  text-align: center;
  color: #546084;
  margin-top: 8px;
  margin-bottom: 16px;
`;

