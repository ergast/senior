import React, { useEffect, useCallback } from 'react';
import { connect } from "react-redux";
import { SafeAreaView } from 'react-native';
import { RootState } from 'src/store/rootReducer';
import { loadRaces, refreshRaces, loadNextPage } from '../../../store/index/actions';
import { isLoadingSelector, isRefreshingSelector, racesSelector, racesTotalSelector } from '../../../store/index/selectors';
import { Race } from '../../../domain/interfaces/Race';


import * as S from "./styles";
import RacesComponentsRacesList from '../../components/races-list';
import { DRIVER_SHOW } from 'src/navigation/constants';
import { useNavigation } from '@react-navigation/native';

type Props = {
  loadRaces: (params?: any) => void
  refreshRaces: (params?: any) => void
  loadNextPage: () => void
  races: Race[]
  totalRaces: number
  isLoading: boolean
  isRefreshing: boolean
};

const RacesPagesIndex = (props: Props) => {
  const { races, isLoading, totalRaces, loadNextPage, isRefreshing, loadRaces, refreshRaces } = props;
  const navigation = useNavigation();

  useEffect(() => {
    loadRaces();
  }, []);

  const onRacePressed = useCallback((race: Race) => {
    navigation.navigate(DRIVER_SHOW, { race });
  }, [navigation]);

  return (
    <SafeAreaView style={{ flex: 1, }}>
      <S.Title>Races { totalRaces != null ? `(${totalRaces})` : '' }</S.Title>
      <S.Subtitle>Select race to show more details</S.Subtitle>
      <RacesComponentsRacesList
        refreshing={isRefreshing}
        loading={isLoading}
        onRefresh={refreshRaces}
        onEndReached={loadNextPage}
        onRacePressed={onRacePressed}
        races={races} />
    </SafeAreaView>
  )
}

const mapStateToProps = (state: RootState) => {
  return {
    isLoading: isLoadingSelector(state),
    isRefreshing: isRefreshingSelector(state),
    races: racesSelector(state),
    totalRaces: racesTotalSelector(state)
  }
}

const mapDispatchToProps = {
  loadRaces,
  refreshRaces,
  loadNextPage
};

export default connect(mapStateToProps, mapDispatchToProps)(RacesPagesIndex);
