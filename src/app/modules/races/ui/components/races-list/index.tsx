import React from 'react';
import { Race } from '../../../domain/interfaces/Race';
import { FlatList, ActivityIndicator } from 'react-native';
import RacesComponentsRacesListItem from '../races-list-item';
import { EmptyListText } from './styles';

type Props = {
  races: Race[]
  loading?: boolean
  onRacePressed: (race: Race) => void
  onEndReached?: () => void
  refreshing?: boolean
  onRefresh?: () => void
  ListHeaderComponent?: any
};

const RacesComponentsRacesList = (props: Props) => {
  const { races, onRacePressed, loading, refreshing, ...otherProps } = props;

  const renderEmptyList = () => {
    if (!loading && !refreshing && races?.length === 0) {
      return (<EmptyListText>List is empty</EmptyListText>);
    }
    return null;
  };

  const renderFooterComponent = () => {
    if (loading && !!races?.length) {
      return <ActivityIndicator color={'#546084'} style={{ marginTop: 20 }} />  
    }
    return null;
  };

  return (
    <FlatList
      {...otherProps}
      data={races}
      keyExtractor={(race: Race) => `${race.date}_${race.raceName}`}
      contentContainerStyle={{ paddingHorizontal: 20, paddingVertical: 16 }}
      refreshing={refreshing}
      ListEmptyComponent={renderEmptyList}
      ListFooterComponent={renderFooterComponent}
      onEndReachedThreshold={0.5}
      renderItem={({ item, index }) => {
        return (<RacesComponentsRacesListItem onPress={onRacePressed} race={item}/>)
      }} />
  )
};

export default RacesComponentsRacesList;