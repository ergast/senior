import React, { useCallback } from 'react';
import { TouchableOpacity, Image } from 'react-native';
import { Race } from '../../../domain/interfaces/Race';

import * as S from './styles';

const IconLink = require('./img/icon-link.png');

type Props = {
  race: Race
  onPress: (race: Race) => void
};

const RacesComponentsRacesListItem = (props: Props) => {
  const { race, onPress } = props;
  const onPressed = useCallback(() => onPress(race), [race]);
  return (
    <TouchableOpacity onPress={onPressed}>
      <S.Card>
        <S.Name>{race.raceName}</S.Name>
        <S.Info>{race.Circuit.Location.country}, {race.Circuit.circuitName}</S.Info>
        <S.Info>{race.date}</S.Info>
        <S.Icon source={IconLink} />
      </S.Card>
    </TouchableOpacity>
  )
};

export default RacesComponentsRacesListItem;