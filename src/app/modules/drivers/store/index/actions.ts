import { createAsyncThunk } from "@reduxjs/toolkit";
import { Driver } from "../../domain/interfaces/Driver";
import driverRepository from "../../domain/api/DriverRepository";
import { BaseResponse } from "src/app/modules/api/interfaces/BaseResponse";
import { metaSelector, isRefreshingSelector } from "./selectors";
import { APIResponseMeta } from "src/app/modules/api/interfaces/APIResponseMeta";
import { RootState } from "src/store/rootReducer";
import { canLoadMore } from "src/app/modules/api/helpers";

export const loadDrivers = createAsyncThunk<BaseResponse<Driver[]>, { offset?: number, limit?: number }>(
  'drivers/index/load',
  async (params = { offset: 0, limit: 20 }, { dispatch }) => {
    return await driverRepository.load(params);
  }
);

export const loadNextPage = createAsyncThunk<BaseResponse<Driver[]|null>, { offset?: number, limit?: number }>(
  'drivers/index/load_next_page',
  async (params, { dispatch, getState, rejectWithValue }) => {
    const state = getState() as RootState;
    const isRefreshing = isRefreshingSelector(state);
    if (!isRefreshing) {
      const meta = metaSelector(state) as APIResponseMeta;
      const canLoad = canLoadMore(meta);
      if (canLoad) {
        const loadParams = { limit: meta.limit, offset: Number(meta.offset) + Number(meta.limit) };
        return await driverRepository.load(loadParams);
      }
    }
    return rejectWithValue(null);
  }
);

export const refreshDrivers = createAsyncThunk<BaseResponse<Driver[]>>(
  'drivers/index/refresh',
  async (params, { dispatch }) => {
    return await driverRepository.load(params);
  }
);