import { createEntityAdapter } from "@reduxjs/toolkit";
import { Driver } from "../../domain/interfaces/Driver";

export const driversAdapter = createEntityAdapter<Driver>({
  selectId: driver => driver.driverId,
});