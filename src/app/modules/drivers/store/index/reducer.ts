import { createSlice } from "@reduxjs/toolkit";
import { driversAdapter } from "./adapter";
import { loadDrivers, refreshDrivers, loadNextPage } from "./actions";
import { APIResponseMeta } from "src/app/modules/api/interfaces/APIResponseMeta";
import { Driver } from "../../domain/interfaces/Driver";

export const driversEntitiesSlice = createSlice({
  name: 'drivers',
  initialState: driversAdapter.getInitialState({
    isLoading: false,
    isRefreshing: false,
    meta: {} as APIResponseMeta
  }),
  reducers: {},
  extraReducers: builder => {
    
    // Load
    builder.addCase(loadDrivers.fulfilled, (state, action) => {
      driversAdapter.addMany(state, action.payload.data);
      state.meta = action.payload.meta;
      state.isLoading = false;
    });
    builder.addCase(loadDrivers.pending, (state, action) => {
      state.isLoading = true;
    });
    builder.addCase(loadDrivers.rejected, (state, action) => {
      state.isLoading = false;
    });

    // Refresh
    builder.addCase(refreshDrivers.fulfilled, (state, action) => {
      driversAdapter.addMany(state, action.payload.data);
      state.meta = action.payload.meta;
      state.isRefreshing = false;
    });
    builder.addCase(refreshDrivers.pending, (state, action) => {
      state.isRefreshing = true;
    });
    builder.addCase(refreshDrivers.rejected, (state, action) => {
      state.isRefreshing = false;
    });

    // Load more
    builder.addCase(loadNextPage.fulfilled, (state, action) => {
      if (action.payload != null) {
        driversAdapter.addMany(state, (action.payload.data as Driver[]));
        state.meta = action.payload.meta;
        console.log('action.payload.meta', action.payload.meta);
      }
      state.isLoading = false;
    });
    builder.addCase(loadNextPage.pending, (state, action) => {
      state.isLoading = true;
    });
    builder.addCase(loadNextPage.rejected, (state, action) => {
      state.isLoading = false;
    });
  }
});

const driversEntitiesReducer = driversEntitiesSlice.reducer;
export default driversEntitiesReducer;
