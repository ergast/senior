import { createSelector } from "@reduxjs/toolkit";

import { RootState } from "src/store/rootReducer";
import { driversStateSelector } from "../selectors";
import { driversAdapter } from "./adapter";

export const driversIndexStateSelector = createSelector(
  driversStateSelector,
  (state) => state.index
)

export const isLoadingSelector = createSelector(
  driversIndexStateSelector,
  (state) => state.isLoading
)

export const isRefreshingSelector = createSelector(
  driversIndexStateSelector,
  (state) => state.isRefreshing
)

export const metaSelector = createSelector(
  driversIndexStateSelector,
  (state) => state.meta
)

export const driversTotalSelector = createSelector(
  metaSelector,
  (state) => (state.total != null) ? Number(state.total) : state.total
)

export const driversSelector = driversAdapter.getSelectors<RootState>(driversIndexStateSelector).selectAll;
export const selectDriverById = driversAdapter.getSelectors<RootState>(driversIndexStateSelector).selectById;
