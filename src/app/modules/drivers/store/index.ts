import { combineReducers } from '@reduxjs/toolkit'
import indexReducer from './index/reducer';

const driversReducer = combineReducers({
  index: indexReducer
});

export type DriversState = ReturnType<typeof driversReducer>

export default driversReducer;
