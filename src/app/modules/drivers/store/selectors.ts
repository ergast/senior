import { RootState } from "src/store/rootReducer";

export const driversStateSelector = (state: RootState) => state.drivers;