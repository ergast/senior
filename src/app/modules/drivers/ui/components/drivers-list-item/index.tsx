import React, { useCallback } from 'react';
import { TouchableOpacity } from 'react-native';
import { Driver } from '../../../domain/interfaces/Driver';

import * as S from './styles';

type Props = {
  driver: Driver
  onPress: (driver: Driver) => void
};

const DriversComponentsDriversListItem = (props: Props) => {
  const { driver, onPress } = props;
  const onPressed = useCallback(() => onPress(driver), [driver]);
  return (
    <TouchableOpacity onPress={onPressed}>
      <S.Card>
        <S.Name>{driver.familyName} {driver.givenName}</S.Name>
        <S.Nationality>{driver.nationality}</S.Nationality>
      </S.Card>
    </TouchableOpacity>
  )
};

export default DriversComponentsDriversListItem;