import styled from 'styled-components';

export const Card = styled.View`
  margin-bottom: 12px;
  background-color: #fff;
  border-radius: 16px;
  padding: 16px 24px;

  box-shadow: 0 0 10px rgba(0,0,0,.1);
`;

export const Name = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: #546084;
`;

export const Nationality = styled.Text`
  font-size: 14px;
  margin-top: 4px;
  color: #546084;
`;