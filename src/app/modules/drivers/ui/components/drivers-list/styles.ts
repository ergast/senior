import styled from 'styled-components';

export const EmptyListText = styled.Text`
  font-size: 14px;
  margin-bottom: 14px;
  text-align: center;
  color: #546084;
  margin-top: 8px;
`;