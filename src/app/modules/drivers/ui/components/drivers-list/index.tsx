import React from 'react';
import { Driver } from '../../../domain/interfaces/Driver';
import { FlatList, ActivityIndicator } from 'react-native';
import DriversComponentsDriversListItem from '../drivers-list-item';
import { EmptyListText } from './styles';

type Props = {
  drivers: Driver[]
  loading?: boolean
  onDriverPressed: (driver: Driver) => void
  onEndReached?: () => void
  refreshing?: boolean
  onRefresh?: () => void
};

const DriversComponentsDriversList = (props: Props) => {
  const { drivers, onDriverPressed, loading, refreshing, ...otherProps } = props;

  const renderEmptyList = () => {
    if (!loading && !refreshing && drivers?.length === 0) {
      return (<EmptyListText>List is empty</EmptyListText>);
    }
    return null;
  };

  const renderFooterComponent = () => {
    if (loading && !!drivers?.length) {
      return <ActivityIndicator color={'#546084'} style={{ marginTop: 20 }} />  
    }
    return null;
  };

  return (
    <FlatList
      {...otherProps}
      data={drivers}
      keyExtractor={(driver: Driver) => driver.driverId}
      contentContainerStyle={{ paddingHorizontal: 20, paddingVertical: 16 }}
      refreshing={refreshing}
      ListEmptyComponent={renderEmptyList}
      ListFooterComponent={renderFooterComponent}
      onEndReachedThreshold={0.5}
      renderItem={({ item, index }) => {
        return (<DriversComponentsDriversListItem onPress={onDriverPressed} driver={item}/>)
      }} />
  )
};

export default DriversComponentsDriversList;