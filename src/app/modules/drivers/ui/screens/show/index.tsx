import React, { useCallback, useEffect } from 'react';
import { Text, SafeAreaView, Linking, TouchableOpacity } from 'react-native';

import * as S from "./styles";
import { StackScreenProps } from '@react-navigation/stack';
import { Driver } from '../../../domain/interfaces/Driver';
import { DRIVER_SHOW } from 'src/navigation/constants';
import { showMessage } from 'react-native-flash-message';
import RacesComponentsRacesList from 'src/app/modules/races/ui/components/races-list';
import { Race } from 'src/app/modules/races/domain/interfaces/Race';
import { connect } from 'react-redux';
import { isLoadingSelector, isRefreshingSelector, racesSelector, racesTotalSelector } from 'src/app/modules/races/store/index/selectors';
import { loadRaces, refreshRaces, loadNextPage } from 'src/app/modules/races/store/index/actions';
import { RootState } from 'src/store/rootReducer';

type ConnectedProps = {
  loadRaces: (params?: any) => void
  refreshRaces: (params?: any) => void
  loadNextPage: () => void
  races: Race[]
  totalRaces: number
  isLoading: boolean
  isRefreshing: boolean
}

type Props = ConnectedProps & StackScreenProps<{ [DRIVER_SHOW]: { driver: Driver } }, 'DRIVER_SHOW'>;

const DriversPagesShow = (props: Props) => {
  const { route } = props;

  const { races, isLoading, totalRaces, loadNextPage, isRefreshing, loadRaces, refreshRaces } = props;

  const driver = route.params?.driver;
  const onDriverLinkPressed = useCallback(async () => {
    const canOpen = await Linking.canOpenURL(driver.url);
    if (canOpen) {
      return Linking.openURL(driver.url);
    } else {
      showMessage({ type: 'danger', message: 'Sorry. Can\'t open link' })
    }
  }, [driver]);

  const onRacePressed = useCallback(async (race: Race) => {
    const canOpen = await Linking.canOpenURL(race.url);
    if (canOpen) {
      return Linking.openURL(race.url);
    } else {
      showMessage({ type: 'danger', message: 'Sorry. Can\'t open link' })
    }
  }, [driver]);

  useEffect(() => {
    loadRaces();
  }, []);


  if (driver != null) {
    return (
      <SafeAreaView style={{ flex: 1, }}>
        <RacesComponentsRacesList
          refreshing={isRefreshing}
          loading={isLoading}
          onRefresh={refreshRaces}
          onEndReached={loadNextPage}
          onRacePressed={onRacePressed}
          races={races}
          ListHeaderComponent={
            <>
              <S.Card>
                <S.Name>{`${driver.familyName} ${driver.givenName}`}</S.Name>
                <S.Nationality>{driver.nationality}</S.Nationality>
                <S.InfoRow>
                  <S.InfoLabel>Birthday</S.InfoLabel>
                  <S.InfoValue>{driver.dateOfBirth}</S.InfoValue>
                </S.InfoRow>
                <S.InfoRow>
                  <S.InfoLabel>Biography link</S.InfoLabel>
                  <TouchableOpacity onPress={onDriverLinkPressed}>
                    <S.InfoLink>Wikipedia</S.InfoLink>
                  </TouchableOpacity>
                </S.InfoRow>
              </S.Card>

              <S.RacesTitle>Driver's races ({ totalRaces })</S.RacesTitle>
            </>
          } />
      </SafeAreaView>
    )
  }

  return <Text>Driver not found</Text>
}

const mapStateToProps = (state: RootState) => {
  return {
    isLoading: isLoadingSelector(state),
    isRefreshing: isRefreshingSelector(state),
    races: racesSelector(state),
    totalRaces: racesTotalSelector(state)
  }
}

const mapDispatchToProps = {
  loadRaces,
  refreshRaces,
  loadNextPage
};

export default connect(mapStateToProps, mapDispatchToProps)(DriversPagesShow);

// export default DriversPagesShow;
