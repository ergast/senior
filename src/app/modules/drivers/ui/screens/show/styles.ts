import styled from 'styled-components';

export const Card = styled.View`
  background-color: #fff;
  border-radius: 16px;
  padding: 16px 24px;
  margin: 16px 0 32px 0;
  box-shadow: 0 0 10px rgba(0,0,0,.1);
`;

export const Name = styled.Text`
  font-size: 18px;
  font-weight: bold;
  color: #546084;
`;

export const Nationality = styled.Text`
  font-size: 16px;
  margin-top: 4px;
  color: #546084;
`;

export const InfoLabel = styled.Text`
  font-size: 14px;
  color: #546084;
`;

export const InfoValue = styled.Text`
  font-size: 16px;
  margin-top: 4px;
  color: #546084;
`;

export const InfoRow = styled.View`
  margin-top: 16px;
`;

export const InfoLink = styled.Text`
  font-size: 16px;
  margin-top: 4px;
  color: #6E68DB;
`;

export const RacesTitle = styled.Text`
  font-size: 18px;
  font-weight: bold;
  text-align: center;
  color: #1E2133;
  margin-bottom: 16px;
`;