import React, { useEffect, useCallback } from 'react';
import { connect } from "react-redux";
import { SafeAreaView } from 'react-native';
import { RootState } from 'src/store/rootReducer';
import { loadDrivers, refreshDrivers, loadNextPage } from '../../../store/index/actions';
import { isLoadingSelector, isRefreshingSelector, driversSelector, driversTotalSelector } from '../../../store/index/selectors';
import { Driver } from '../../../domain/interfaces/Driver';


import * as S from "./styles";
import DriversComponentsDriversList from '../../components/drivers-list';
import { DRIVER_SHOW } from 'src/navigation/constants';
import { useNavigation } from '@react-navigation/native';

type Props = {
  loadDrivers: (params?: any) => void
  refreshDrivers: (params?: any) => void
  loadNextPage: () => void
  drivers: Driver[]
  totalDrivers: number
  isLoading: boolean
  isRefreshing: boolean
};

const DriversPagesIndex = (props: Props) => {
  const { drivers, isLoading, totalDrivers, loadNextPage, isRefreshing, loadDrivers, refreshDrivers } = props;
  const navigation = useNavigation();

  useEffect(() => {
    loadDrivers();
  }, []);

  const onDriverPressed = useCallback((driver: Driver) => {
    navigation.navigate(DRIVER_SHOW, { driver });
  }, [navigation]);

  return (
    <SafeAreaView style={{ flex: 1, }}>
      <S.Title>Drivers { totalDrivers != null ? `(${totalDrivers})` : '' }</S.Title>
      <S.Subtitle>Select driver to show more details</S.Subtitle>
      <DriversComponentsDriversList
        refreshing={isRefreshing}
        loading={isLoading}
        onRefresh={refreshDrivers}
        onEndReached={loadNextPage}
        onDriverPressed={onDriverPressed}
        drivers={drivers} />
    </SafeAreaView>
  )
}

const mapStateToProps = (state: RootState) => {
  return {
    isLoading: isLoadingSelector(state),
    isRefreshing: isRefreshingSelector(state),
    drivers: driversSelector(state),
    totalDrivers: driversTotalSelector(state)
  }
}

const mapDispatchToProps = {
  loadDrivers,
  refreshDrivers,
  loadNextPage
};

export default connect(mapStateToProps, mapDispatchToProps)(DriversPagesIndex);
