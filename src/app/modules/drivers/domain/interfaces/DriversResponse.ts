import { APIResponseMeta } from "src/app/modules/api/interfaces/APIResponseMeta";
import { Driver } from "./Driver";

export interface DriversResponse extends APIResponseMeta {
  DriverTable: {
      circuitId: string
      constructorId: string
      Drivers: Array<Driver>
  }
}