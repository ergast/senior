import { BaseRepository } from "src/app/modules/api/BaseRepository";
import { DriversResponse } from "../interfaces/DriversResponse";
import { Driver } from "../interfaces/Driver";
import { AxiosResponse } from "axios";
import { APIResponse } from "src/app/modules/api/interfaces/APIResponse";
import httpResource from "src/app/modules/api/httpResource";

class DriverRepository extends BaseRepository<DriversResponse, Driver> {
  protected extractListFromResponse(response: AxiosResponse<APIResponse<DriversResponse>>): Driver[] {
    return response?.data?.MRData?.DriverTable?.Drivers;
  }
}

const driverRepository = new DriverRepository(httpResource, 'drivers');
export default driverRepository;