import { APIResponseMeta } from "./interfaces/APIResponseMeta";

export const canLoadMore = (meta: APIResponseMeta): boolean => {
  const { total, offset, limit } = meta;
  if (offset != null && limit != null && total != null) {
    return Number(offset) + Number(limit) < Number(total);
  }
  return false;
};