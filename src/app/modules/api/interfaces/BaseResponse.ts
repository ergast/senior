export interface BaseResponse<T> {
  data: T
  meta: {
    xmlns?: string
    series?: string
    limit?: string
    offset?: string
    total?: string
  }
}