export interface APIResponseMeta {
  xmlns?: string
  series?: string
  limit?: string
  offset?: string
  total?: string
}

// limit: "10"
// offset: "0"
// series: "f1"
// total: "848"
// url: "http://ergast.com/api/f1/drivers.json"
// xmlns: "http://ergast.com/mrd/1.4"