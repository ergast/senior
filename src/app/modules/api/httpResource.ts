import axios from "axios";
import Config from "react-native-config";

const httpResource = axios.create({ baseURL: Config.API_URL });
export default httpResource;