import { AxiosInstance, AxiosResponse } from "axios";
import { APIResponse } from "./interfaces/APIResponse";
import { BaseResponse } from "./interfaces/BaseResponse";

export class BaseRepository<R, Entity> {
  constructor(protected resource: AxiosInstance, protected relativePath: string) { }

  public load(params?: any): Promise<BaseResponse<Entity[]>> {
    return this.resource.get(`${this.relativePath}.json`, { params })
      .then((response: AxiosResponse<APIResponse<R>>) => {
        const result = this.extractListFromResponse(response);
        return this.processResponse<Entity[]>(response, result);
      })
  }

  public loadById(id: number | string): Promise<BaseResponse<Entity> | null> {
    return this.resource.get(`${this.relativePath}/${id}.json`)
      .then((response: AxiosResponse<APIResponse<R>>) => {
        const list = this.extractListFromResponse(response);
        if (list?.length > 0) {
          return this.processResponse(response, list[0]);
        }
        return null;
      });
  }

  protected extractListFromResponse(response: AxiosResponse<APIResponse<R>>): Entity[] {
    return (response.data as any).MRData as Entity[];
  }

  protected processResponse<Result>(response: AxiosResponse<APIResponse<R>>, data: any): BaseResponse<Result> {
    const metaSource = response.data.MRData as any || {};
    return {
      data,
      meta: {
        xmlns: metaSource.xmlns,
        series: metaSource.series,
        limit: metaSource.limit,
        offset: metaSource.offset,
        total: metaSource.total
      }
    } as BaseResponse<Result>;
  }
}